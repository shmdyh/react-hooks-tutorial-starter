import React, { useState, useEffect } from "react";
import "./App.css";
import { BookToRead } from "./BookToRead";
import BookRow from "./BookRow";
import Modal from 'react-modal';
import BookSearchDialog from './BookSearchDialog';
import { BookDescription } from "./BookDescription";

const APP_KEY = "react-hooks-tutorial"

Modal.setAppElement('#root');

const customStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.8)"
  },
  content: {
    top:"50%",
    left:"50%",
    right:"auto",
    bottom:"auto",
    marginRight:"-50%",
    padding:0,
    transform:"translate(-50%, -50%)"
  }
};

const App:React.FC = () => {
  const [books, setBooks]:[
    BookToRead[],
    React.Dispatch<React.SetStateAction<BookToRead[]>>
    ]  = useState<BookToRead[]>([] as BookToRead[]);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  useEffect(() => {
    const stragedBooks = localStorage.getItem(APP_KEY);
    if (stragedBooks) {
      setBooks(JSON.parse(stragedBooks));
    }
  },[]);

  useEffect(() => {
    localStorage.setItem(APP_KEY, JSON.stringify(books));
  },[books]);

  const handleBookDelete = (id: number) => {
    const newBooks = books.filter((b) => {return b.id !== id});
    setBooks(newBooks);
  };

  const handleBookMemoChange = (id: number, memo: string) => {
    const newBooks = books.map((b) => {
      return b.id === id ?
      { ...b, memo:memo }
      :b;
    });
      setBooks(newBooks);
  };

  const handleAddClick = () => {
    setModalIsOpen(true);
  }

  const handleModalClose = () => {
    setModalIsOpen(false);
  }

  const handleBookAdd = (book: BookDescription) => {
    const newBook: BookToRead = {...book, id: books.length + 1, memo:"" };
    const newBooks = [...books, newBook];
    setBooks(newBooks);
    setModalIsOpen(false);
  }

  const booksRows:React.ReactElement[] = books.map((b) => {
    return (
      <BookRow
      book={b}
      key={b.id}
      onMemoChange={(id:number, memo:string) => {handleBookMemoChange(id, memo)}}
      onDelete={(id:number) => {handleBookDelete(id)}}
      />
    );
  });

  return (
    <div className="App">
      <section className="nav">
        <h1>読みたい本リスト</h1>
        <div className="button-like" onClick={handleAddClick}>
          本を追加
        </div>
      </section>
      <section className="main">
        {booksRows}
      </section>
      <Modal
      isOpen={modalIsOpen}
      onRequestClose={handleModalClose}
      style={customStyles}
      >
        <BookSearchDialog maxResults={20} onBookAdd={(b) => handleBookAdd(b)} />
      </Modal>
    </div>
  );
};

export default App;
